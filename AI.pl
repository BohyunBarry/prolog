path([state(X, 5)|Xs],[state(X, 5)|Xs]):- !.
path([X|Xs],Rs):-
    move(X,Y),not(member(Y,[X|Xs])),
    path([Y,X|Xs],Rs).

puzzle:- path([state(0,0)],PathList),
    write(PathList), nl, fail.

move(s(X,Y),s(Z,7),OpenList,ClosedList) :- Z is X - (7 - Y), Z >=0, not(member(s(Z,7), OpenList)), not(member(s(Z,7), ClosedList)) ,write("4L into 7L. now at " + [Z , 7]), nl.
move(s(X,Y),s(Z,0),OpenList,ClosedList) :- Z is X + Y, Z =< 4, Z>0, not(member(s(Z,0), OpenList)), not(member(s(Z,0), ClosedList)), write("7L into 4L. now at " + [Z , 0]), nl.
move(s(X,Y),s(4,Z),OpenList,ClosedList) :- Z is Y - (4 - X), Z >=0, not(member(s(4,Z), OpenList)), not(member(s(4,Z), ClosedList)), write("7L into 4L. now at " +  [4 , Z]), nl.
move(s(X,Y),s(0,Z),OpenList,ClosedList) :- Z is X + Y, Z =< 7, Z>0, not(member(s(0,Z), OpenList)), not(member(s(0,Z), ClosedList)), write("4L into 7L. now at "  + [0, Z]), nl.

move(s(0,Y),s(4,Y),OpenList,ClosedList) :- not(member(s(4,Y), OpenList)), not(member(s(4,Y), ClosedList)), write("4L filled. now at " + [4 , Y]), nl.
move(s(X,0),s(X,7),OpenList,ClosedList) :- not(member(s(X,7), OpenList)), not(member(s(X,7), ClosedList)), write("7L filled. now at " + [X , 7]), nl.
move(s(X,Y),s(X,0),OpenList,ClosedList) :- not(member(s(X,0), OpenList)), not(member(s(X,0), ClosedList)), Y > 0, write("7L emptied. now at " + [X , 0]), nl.
move(s(X,Y),s(0,Y),OpenList,ClosedList) :- not(member(s(0,Y), OpenList)), not(member(s(0,Y), ClosedList)),X > 0, write("4L emptied. now at " + [0 , Y]), nl.

msolution(s(X,Y),s(Z,7)) :- Z is X - (7 - Y), Z >= 0, write("4L into 7L. now at " + [Z , 7]), nl.
msolution(s(X,Y),s(Z,0)) :- Z is X + Y, Z =< 4, Z>0, write("7L into 4L. now at " + [Z , 0]), nl.
msolution(s(X,Y),s(4,Z)) :- Z is Y - (4 - X), Z >=0, write("7L into 4L. now at " +  [4 , Z]), nl.
msolution(s(X,Y),s(0,Z)) :- Z is X + Y, Z =< 7, Z>0, write("4L into 7L. now at "  + [0, Z]), nl.

msolution(s(0,Y),s(4,Y)) :- write("4L filled. now at " + [4 , Y]), nl.
msolution(s(X,0),s(X,7)) :- write("7L filled. now at " + [X , 7]), nl.
msolution(s(X,Y),s(X,0)) :- Y > 0, write("7L emptied. now at " + [X , 0]), nl.
msolution(s(X,Y),s(0,Y)) :- X > 0, write("4L emptied. now at " + [0 , Y]), nl.

moves(Path, OpenList, ClosedList) :- moves([s(0,0)],Path,OpenList,ClosedList).

moves([s(X0,Y0)|T], Path, OpenList, ClosedList) :- 
    msolution(s(X0,Y0),s(X1,5)),
    write("SOLUTION FOUND"), 
    nl,
    write("Path taken for this solution: " + [s(X1,5),s(X0,Y0)|T]),
    nl, 
    write("Open List: " + OpenList),
    nl,
    write("Closed List: " + ClosedList),
    nl,
    write("Continuing search for additional solutions"),
    nl,
    !.

moves([s(X0,Y0)|T], Path, OpenList, ClosedList) :-
    % adds to the closed list 
    add(s(X0,Y0), ClosedList, ClosedListNew),
    move(s(X0,Y0),s(X1,Y1), OpenList, ClosedList), 
    not(member(s(X1,Y1),[s(X0,Y0)|T])),
    %bagof(S, move(s(X0,Y0),s(X1,Y1), Path, OpenList, ClosedList), TempList);write(TempList),empty_list(TempList),
    %write(TempList),
    %not(member(s(X1,Y1),[s(X0,Y0)|T])), not(member(s(X1,Y1), OpenList)), not(member(s(X1,Y1), ClosedList)),
    % adds to the openlist
    add(s(X1,Y1), OpenList, OpenListNew),
    moves([s(X1,Y1),s(X0,Y0)|T], Path, OpenListNew, ClosedListNew). 
		
solve :- 
    empty_list(Path),
    empty_list(OpenList),
    empty_list(ClosedList),
    moves(Path, OpenList, ClosedList), 
    nl,
    fail.

empty_list([]).

    % simply adds an item to a list
add(X, S, S) :- member(X, S), !.
add(X, S, [X|S]).
