move(s(X,Y,OP),s(Z,7,P),Path,OpenList,ClosedList) :- Z is X - (7 - Y), Z >=0, P is OP + 1, not(member_state(s(Z,7,P), OpenList)), not(member_state(s(Z,7,P), ClosedList)), not(member_state(s(Z,7,P), Path)), write("4L into 7L. added to openList: " + [Z , 7]), nl.
move(s(X,Y,OP),s(Z,0,P),Path,OpenList,ClosedList) :- Z is X + Y, Z =< 4, Z>0, P is OP + 1, not(member_state(s(Z,0,P), OpenList)), not(member_state(s(Z,0,P), ClosedList)), not(member_state(s(Z,0,P), Path)), write("7L into 4L. added to openList: " + [Z , 0]), nl.
move(s(X,Y,OP),s(4,Z,P),Path,OpenList,ClosedList) :- Z is Y - (4 - X), Z >=0, P is OP + 1, not(member_state(s(4,Z,P), OpenList)), not(member_state(s(4,Z,P), ClosedList)), not(member_state(s(4,Z,P), Path)), write("7L into 4L. added to openList: " + [4 , Z]), nl.
move(s(X,Y,OP),s(0,Z,P),Path,OpenList,ClosedList) :- Z is X + Y, Z =< 7, Z>0, P is OP + 1, not(member_state(s(0,Z,P), OpenList)), not(member_state(s(0,Z,P), ClosedList)), not(member_state(s(0,Z,P), Path)), write("4L into 7L. added to openList: "  +[0 , Z]), nl.

move(s(0,Y,OP),s(4,Y,P),Path,OpenList,ClosedList) :- not(member_state(s(4,Y,P), Path)), not(member_state(s(4,Y,P), OpenList)), not(member_state(s(4,Y,P), ClosedList)), P is OP + 1, write("4L filled. added to openList: " + [4 , Y]), nl.
move(s(X,0,OP),s(X,7,P),Path,OpenList,ClosedList) :- not(member_state(s(X,7,P), Path)), not(member_state(s(X,7,P), OpenList)), not(member_state(s(X,7,P), ClosedList)), P is OP + 1, write("7L filled. added to openList: " + [X , 7]), nl.
move(s(X,Y,OP),s(X,0,P),Path,OpenList,ClosedList) :- not(member_state(s(X,0,P), Path)), not(member_state(s(X,0,P), OpenList)), not(member_state(s(X,0,P), ClosedList)), P is OP + 1, Y > 0, write("7L emptied. added to openList: " + [X , 0]), nl.
move(s(X,Y,OP),s(0,Y,P),Path,OpenList,ClosedList) :- not(member_state(s(0,Y,P), Path)), not(member_state(s(0,Y,P), OpenList)), not(member_state(s(0,Y,P), ClosedList)), P is OP + 1, X > 0, write("4L emptied. added to openList: " + [0 , Y]), nl.

sol_move(s(X,Y,OP),s(Z,7,P),Path) :- Z is X - (7 - Y), Z >=0, P is OP + 1, not(member_state(s(Z,7,P), Path)), write("solution found at " + [Z , 7]), nl.
sol_move(s(X,Y,OP),s(Z,0,P),Path) :- Z is X + Y, Z =< 4, Z>0, P is OP + 1, not(member_state(s(Z,0,P), Path)), write("solution found at " + [Z , 0]), nl.
sol_move(s(X,Y,OP),s(4,Z,P),Path) :- Z is Y - (4 - X), Z >=0, P is OP + 1, not(member_state(s(4,Z,P), Path)), write("solution found at " + [4 , Z]), nl.
sol_move(s(X,Y,OP),s(0,Z,P),Path) :- Z is X + Y, Z =< 7, Z>0, P is OP + 1, not(member_state(s(0,Z,P), Path)), write("solution found at "  +[0 , Z]), nl.

%sol_move(s(0,Y,OP),s(4,Y,P),Path) :- not(member_state(s(4,Y,P), Path)), P is OP + 1, write("sol 4L filled. now at " + [4 , Y]), nl.
%sol_move(s(X,0,OP),s(X,7,P),Path) :- not(member_state(s(X,7,P), Path)), P is OP + 1, write("sol 7L filled. now at " + [X , 7]), nl.
%sol_move(s(X,Y,OP),s(X,0,P),Path) :- not(member_state(s(X,0,P), Path)), P is OP + 1, Y > 0, write("sol 7L emptied. now at " + [X , 0]), nl.
%sol_move(s(X,Y,OP),s(0,Y,P),Path) :- not(member_state(s(0,Y,P), Path)), P is OP + 1, X > 0, write("sol 4L emptied. now at " + [0 , Y]), nl.

solve() :- 
    empty_list(OpenList),
    moves([s(0,0,0)],OpenList,[s(0,0,0)]),
    nl,
    write("All possible solutions found!"). 

%moves([s(X0,Y0,P0)|T], OpenList, ClosedList) :-
    %write([s(X0,Y0,P0)|T]),nl,
    %write(OpenList),nl,
    %write(ClosedList),nl,
    %fail.

moves(Path, OpenList, ClosedList) :- 
    [s(X0,Y0,P0),s(X2,Y2,P2)|T] = Path,
    sol_move(s(X0,Y0,P0),s(X1,5,P1), [s(X0,Y0,P0),s(X2,Y2,P2)|T]),
    %back_trace(OpenList, ClosedList, T, PathNew, OpenListNew, ClosedListNew),
    write("SOLUTION FOUND"), 
    nl,
    write("Path taken for this solution: " + [s(X1,5,P1)|Path]),
    %nl,
    %write("NEW PATH: " + PathNew),
    nl, 
    write("Open List: " + OpenList),
    nl,
    write("Closed List: " + ClosedList),
    nl,
    write("Continuing search for additional solutions"),
    nl,nl,
    %check if open list is empty
    OpenList == []->
    !;
    fail.

moves([s(X0,Y0,P0)|T], OpenList, ClosedList) :-
    %write("recursion"),
    %% check if next node reaches goal, if so backtrace.
    sol_move(s(X0,Y0,P0),s(X1,5,P1), [s(X0,Y0,P0)|T])->

    %write([s(X0,Y0,P0)|T]),nl,
    %write(OpenList),nl,
    %write(ClosedList),nl,
    
    
    % Collects all possible solutions right now.
    %bagof(s(X1,Y1,P1), move(s(X0,Y0,P0),s(X1,Y1,P1), [s(X0,Y0,P0)|T], OpenList, ClosedList), TempList),

    % reverse the list, add the list to the open list
    %conc(TempList, OpenList, OpenListTemp),
    back_trace(OpenList, ClosedList, T, PathNew, OpenListNew, ClosedListNew),
    
    % adding OpenListNew automatically pops the first value off the open list.
    moves(PathNew, OpenListNew, ClosedListNew);

    % find the next node (if possible)
    % Collects all possible solutions right now.
    bagof(s(X1,Y1,P1), move(s(X0,Y0,P0),s(X1,Y1,P1), [s(X0,Y0,P0)|T], OpenList, ClosedList), TempList),

    % reverse the list, add the list to the open list
    conc(TempList, OpenList, OpenListTemp),

    % creates new path, open and closed lists
    fetch_next_node(OpenListTemp, ClosedList, [s(X0,Y0,P0)|T], PathNew, OpenListNew, ClosedListNew),
    moves(PathNew, OpenListNew, ClosedListNew);

    % backtrace to the next possible node.
    % Collects all possible solutions right now.
    %bagof(s(X1,Y1,P1), move(s(X0,Y0,P0),s(X1,Y1,P1), [s(X0,Y0,P0)|T], OpenList, ClosedList), TempList),

    % reverse the list, add the list to the open list
    %conc(TempList, OpenList, OpenListTemp),
    back_trace(OpenList, ClosedList, [s(X0,Y0,P0)|T], PathNew, OpenListNew, ClosedListNew),
    
    % adding OpenListNew automatically pops the first value off the open list.
    moves(PathNew, OpenListNew, ClosedListNew). 

empty_list([]).

member_state(s(X0,Y0,_), [s(X0,Y0,_)|_]). % check list is not empty. run recursion
member_state(s(X0,Y0,_),[_|L]) :-
    member_state(s(X0,Y0,_),L).
    %member_state(s(X0,Y0,_),L).

    %  OpenList, ClosedList, path, return path, OpenListNew, ClosedListNew
%back_trace([s(X0,Y0,_)|T], _, [s(_,_,P0)|L], [s(X0,Y0,P0)|L], [s(X0,Y0,P0)|T], [s(X0,Y0,P0)]).
back_trace([s(X0,Y0,P0)|T], D, [s(_,_,P1)|L], ReturnPath, ReturnOpenList, ReturnClosedList) :-
    P2 is (P1 - 1),
    P0 == P1,
    ReturnPath = [s(X0,Y0,P0)|L],
    ReturnOpenList = T,
    ReturnClosedList = [s(X0,Y0,P0)|D].
    
    % recursion to find node
back_trace([s(X0,Y0,P0)|T], ClosedList, [_|L],ReturnPath, ReturnOpenList, ReturnClosedList) :-
    back_trace([s(X0,Y0,P0)|T], ClosedList, L,ReturnPath, ReturnOpenList, ReturnClosedList).

    %  OpenList, ClosedList, path, return path, OpenListNew, ClosedListNew
fetch_next_node([s(X0,Y0,P0)|T], D, [s(X1,Y1,P1)|L], ReturnPath, ReturnOpenList, ReturnClosedList) :-
    P2 is (P1 + 1),
    P0 == P2,
    ReturnPath = [s(X0,Y0,P0),s(X1,Y1,P1)|L],
    ReturnClosedList = [s(X0,Y0,P0)|D],
    ReturnOpenList = T.
    % recursion to find node
fetch_next_node([s(X0,Y0,P0)|T], ClosedList, [_|L],ReturnPath, ReturnOpenList, ReturnClosedList) :-
    fetch_next_node([s(X0,Y0,P0)|T], ClosedList, L,ReturnPath, ReturnOpenList, ReturnClosedList).

    % adds the first element (L1) of a list (L2) to another list (L) and returns (R)
add_first([L1|_],L,R) :-
    add(L1,L,R).

    % reverses a list (L) and returns it as R 
reverse(L,R) :-
    rev(L,[],R).
rev([],R,R).
rev([H|T], L, R) :-
    rev(T,[H|L],R).

    % deletes a value from a list
del(X,[X|T],T).
del(X,[H|T],[H|A]) :-
    del(X,T,A).

    % appends a list to another list (unless it is already in the list)
conc([], List, List).
conc([Head|Tail], List, [Head|Rest]) :-
    conc(Tail, List, Rest).
%conc([], S, S).
%conc([H|T], S, S_new) :- 
    %add(H, S2, S_new),
    %conc(T, S, S2).  

    % simply adds an item to a list
add(X, S, S) :- member_state(X, S), !.
add(X, S, [X|S]).


